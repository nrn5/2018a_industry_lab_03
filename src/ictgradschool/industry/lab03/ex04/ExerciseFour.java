package ictgradschool.industry.lab03.ex04;

import ictgradschool.Keyboard;

import java.security.PrivateKey;

/**
 * Write a program that prompts the user to enter an amount and a number of decimal places.  The program should then
 * truncate the amount to the user-specified number of decimal places using String methods.
 *
 * <p>To truncate the amount to the user-specified number of decimal places, the String method indexOf() should be used
 * to find the position of the decimal point, and the method substring() should then be used to extract the amount to
 * the user-specified number of decimal places.  The program is to be written so that each task is in a separate method.
 * You need to write four methods, one method for each of the following tasks:</p>
 * <ul>
 *     <li>Printing the prompt and reading the amount from the user</li>
 *     <li>Printing the prompt and reading the number of decimal places from the user</li>
 *     <li>Truncating the amount to the user-specified number of DP's</li>
 *     <li>Printing the truncated amount</li>
 * </ul>
 */
public class ExerciseFour {

    private void start() {
        // TODO Use other methods you create to implement this program's functionality.
        String number = getAmountFromUser();
        int decimalPlaces = getDecimalPlacesFromUser();
        String truncatedNumber = getTruncatedAmount(number, decimalPlaces);
        printTruncatedAmount(truncatedNumber, decimalPlaces);
    }
    private String getAmountFromUser() {
        // TODO Write a method which prompts the user and reads the amount to truncate from the Keyboard
        System.out.println("Please enter an amount: ");
        String amount;
        amount = Keyboard.readInput();
        return amount;
    }
    // TODO Write a method which prompts the user and reads the number of DP's from the Keyboard
    private int getDecimalPlacesFromUser() {
        System.out.println("Please enter the number of decimal places: ");
        String decimalPlaces;
        decimalPlaces = Keyboard.readInput();
        return Integer.parseInt(decimalPlaces);
    }
    // TODO Write a method which truncates the specified number to the specified number of DP's
private String getTruncatedAmount(String number, int dp) {
        /*String decimalPlaces;
        decimalPlaces = getDecimalPlacesFromUser()
        String truncatedAmount;
        truncatedAmount = getAmountFromUser().substring(getDecimalPlacesFromUser());*/
        int indexOfDecimal = number.indexOf(".");
        String trunca = number.substring(0, indexOfDecimal + dp+1);
        return trunca;
    }

    // TODO Write a method which prints the truncated amount
    public void printTruncatedAmount(String amount, int decimalPlaces) {
        System.out.println("Amount truncated to " + decimalPlaces + " decimal places is: " + amount);
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {
        ExerciseFour ex = new ExerciseFour();
        ex.start();
    }
}
