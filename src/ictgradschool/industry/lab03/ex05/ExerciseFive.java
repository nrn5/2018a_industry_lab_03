package ictgradschool.industry.lab03.ex05;

import com.sun.deploy.security.WIExplorerBrowserAuthenticator14;
import com.sun.rowset.internal.Row;

/**
 * Created by anhyd on 3/03/2017.
 */
public class ExerciseFive {

    /**
     * Runs an example program.
     */
    private void start() {
        String letters = "X X O O X O X O X ";

        String row1 = getRow(letters, 1);

        String row2 = getRow(letters, 2);

        String row3 = getRow(letters, 3);

        printRows(row1, row2, row3);

        String leftDiagonal = getLeftDiagonal(row1, row2, row3);

        printDiagonal(leftDiagonal);
    }

    /**
     * TODO Implement this
     */
    public String getRow(String letters, int row) {
        String row1, row2, row3;

        row1 = letters.substring(0, 6);
        row2 = letters.substring(6, 12);
        row3 = letters.substring(12);

        if (row == 1) {
            return row1;
        } else if (row == 2) {
            return row2;
        } else {
            return row3;
        }
    }

    /**
     * TODO Implement this
     */
    public void printRows(String row1, String row2, String row3) {
        System.out.println(row1);
        System.out.println(row2);
        System.out.println(row3);
    }

    /**
     * TODO Implement this
     */
    public String getLeftDiagonal(String row1, String row2, String row3) {
        row1 = row1.substring(0, 1);
        row2 = row2.substring(2, 3);
        row3 = row3.substring(4, 5);

        return row1 + " " + row2 + " " + row3;
    }

    /**
     * TODO Implement this
     */
    public void printDiagonal(String leftDiagonal) {
        System.out.println("Diagonal: " + leftDiagonal);
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {
        ExerciseFive ex = new ExerciseFive();
        ex.start();
    }
}
