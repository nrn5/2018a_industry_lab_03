package ictgradschool.industry.lab03.ex02;

import ictgradschool.Keyboard;

/**
 * Write a program that prompts the user to enter a range – 2 integers representing a lower bound and an upper bound.
 * You should use Keyboard.readInput() for this. Then, convert these bounds from String to int using Integer.parseInt().
 * Your program should then use Math.random() to generate 3 random integers that lie between the range entered (inclusive),
 * and then use Math.min() to determine which of the random integers is the smallest.
 */
public class ExerciseTwo {

    /**
     * TODO Your code here. You may also write additional methods if you like.
     */
    private void start() {

        System.out.print("Enter two whole numbers to represent a range. One representing a lower band and one an upper band");
        String str1, str2;

        str1 = Keyboard.readInput();
        str2 = Keyboard.readInput();

        int num1, num2;

        num1 = Integer.parseInt(str1);
        num2 = Integer.parseInt(str2);

        int lowerBound, upperBound;

        lowerBound = Math.min (num1, num2);
        upperBound = Math.max(num1, num2);

        int randomNumber1, randomNumber2, randomNumber3;

        randomNumber1 = (int) (lowerBound + Math.round(Math.random() * (upperBound - lowerBound)));
        randomNumber2 = (int) (lowerBound + Math.round(Math.random() * (upperBound - lowerBound)));
        randomNumber3 = (int) (lowerBound + Math.round(Math.random() * (upperBound - lowerBound)));

        int smallestInteger;

        System.out.println(randomNumber1);
        System.out.println(randomNumber2);
        System.out.println(randomNumber3);

        smallestInteger = Math.min(Math.min (randomNumber1, randomNumber2), randomNumber3);
System.out.print(smallestInteger);
/*
step 1 while Keyboard.readInput == "" promt to enter two whole numbers, one representing a lower bound and one an upper bound
step 2 Store as String num1 and String num2
step 3 Integer.parseInt (int num1, int num2)
step 4 Math.min (num1, num2) = lower bound
        other num = upper bound
step 5 Math.random ( randomNumber1 = >= lower bound && <=upper bound
        Math.random ( randomNumber2 = >= lower bound && <=upper bound
        Math.random ( randomNumber3 = >= lower bound && <=upper bound
        smallestInteger = Math.min(Math.min (randomNumber1, randomNumber2), randomNumber3);
 */
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        ExerciseTwo ex = new ExerciseTwo();
        ex.start();

    }
}
